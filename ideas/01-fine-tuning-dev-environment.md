# Fine tuning dev environment

I collected a few action points for us to discuss in order to smoothen our dev experience.

## Format and Linting

Delegate/automate formatting to Prettier and disconnect it from eslint.

@TODO: shall we enforce any specific Prettier config? Any preference?

## Source code and distribution

Currently we distribute our bundled via the source code repository itself (committing the `dist` folder). This has a few drawbacks:

- Increase of repo size (`payment-component` is 100MB)
- Pull requests, commit diff clutter
- GIT repos can be slow to download and can break/slow down our consumer's dependency installation process

#### Proposal

Consider moving compiled code out of version control and use a Private NPM registry (maybe GitLab's?) to distribute our bundled code.

## Smoothing project installation/setup

The only dependency of our repos is Node.js, but it's not clear which version each repo depends on. This can lead to several kind of failures.

#### Proposal (not mutual exclusive)

- Add an engine field to [`package.json`](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#engines)
- Add a [`.nvmrc` file](https://github.com/nvm-sh/nvm#nvmrc) to each repo to automate `nvm` setup for every specific project
- Find a proper strategy to dockerize the projects (dependecies install and dev server startup, to begin with)
- Fill read me with necessary install steps

## Documentation

Current documentation is published to Confluence. Drawbacks:

- Confluence :)
- Documentation can turn easily outdated and no one noticing (or have time to update it)
- Hard to gather relevant information

#### Proposal

Bring documentation under version control. This would enforce updating docs directly in relevant PR's and would better communicate what is documented and what is not.

## Test automation

I haven't found any CI pipeline responsible for running tests. This will be vital as soon as we'll start to test cover our projects more thoroughly.

#### Proposal

We might pick up a CI service of our choice, and begin with automating tests execution at every PR and master commit.

Do BitBucket and GitLab provide any integrated CI tool or any integration for external CI services?

## Shared code

Payments and Orderbox currently share a conisistent set of code, depencies and tools. Currently eveything is provided as a `npm` dependencies

#### Proposal

We might consider reducing authoring and maintenance effort, by moving all/most of our repos to a monorepo. A good opportunity of doing so might be the transition from Bitbucket to Gitlab.


## Comments

Comments available here #1.
